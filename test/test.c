#include <check.h>

unsigned long fact(unsigned char a);

START_TEST (test4) {
    ck_assert_int_eq(fact(4), 24);
}
END_TEST

START_TEST (test6) {
    ck_assert_int_eq(fact(6), 720);
}
END_TEST

START_TEST (test21) {
    ck_assert_int_eq(fact(21), -1);
}
END_TEST

START_TEST (test0) {
    ck_assert_int_eq(fact(0), 1);
}
END_TEST

START_TEST (test1) {
    ck_assert_int_eq(fact(1), 1);
}
END_TEST

Suite * fact_suite() {
    Suite* suite = suite_create("Factorial Test");
    TCase* core = tcase_create("Core");
    //suite = suite_create("Factorial Test");
    //core = tcase_create("Core");
    tcase_add_test(core, test0);
    tcase_add_test(core, test1);
    tcase_add_test(core, test4);
    tcase_add_test(core, test6);
    tcase_add_test(core, test21);
    suite_add_tcase(suite, core);
    return suite;
}

int main() {
    int failed;
    Suite* suite = fact_suite();
    SRunner* runner = srunner_create(suite);
    //suite = fact_suite();
    //runner = srunner_create(suite);
    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    
    return failed == 0 ? 0 : 1;
}
