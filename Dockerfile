FROM ubuntu:22.04
WORKDIR /PPC
COPY . .
RUN apt-get update -qq && apt-get install -y -qq gcc make dpkg
RUN dpkg -i factorial.deb
ENTRYPOINT ["factorial"]
