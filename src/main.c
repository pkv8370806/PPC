#include <stdlib.h>
#include <stdio.h>
#include <stdio_ext.h>

unsigned long fact(unsigned char a);

int main (int argc, char* argv[]) {
	unsigned char x = 0;
	unsigned long r = -1;
	if (argc == 1) {
		while (1) {
			printf("Enter the number you want a factorial of_ ");
			if (scanf("%hhu", &x) == 1) {
				r = fact(x);
				if (r != -1) break;
			}
			printf("It must be a whole non-negative number less than 21\n\n");
			__fpurge(stdin);
		}
	}
	else if (argc == 2) {
		x = atoi(argv[1]);
		r = fact(x);
		if (r == -1) {
			printf("The program accepts only a whole non-negative number less than 21\n");
			return -1;
		}
	}
	else {
		printf("The program accepts only 1 argument - a whole non-negative number less than 21\n");
		return -1;
	}
	printf("%u! = %lu\n", x, r);
	return 0;
}
