unsigned long fact(unsigned char a) {
	if (a > 20 || a < 0) return -1;
	if (a == 1 || a == 0) return 1;
	unsigned long b = fact(a - 1);
	return a * b;
}
